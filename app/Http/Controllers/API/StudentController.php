<?php

namespace App\Http\Controllers\API;

use App\Models\Student;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\StudentResource;
use Illuminate\Support\Facades\Storage;

class StudentController extends Controller
{
    public function update(Request $request)
    {
        $data = $request->validate([
            'id_school_level' => 'required',
            'long_name' => 'required|string',
            'address' => 'required',
            'school_name' => 'required',
            'class' => 'required',
            'photo_student' => 'mimes:jpeg,png,jpg,svg|max:2048',
        ]);

        $now = date("Y-m-d H:i:s");
        $slug = Str::slug($request->long_name . ' ' . $now);

        if($request->file('photo_student')) {
            Storage::delete($request->user()->photo_student);
            
            $photo = $request->file('photo_student');
            $photoUrl = $photo->storeAs("APIDocs/student/photos", "{$slug}.{$photo->extension()}");
        } else {
            $photoUrl = null;
        }

        $data['photo_student'] = $photoUrl;

        $request->user()->update($data);

        return response()->json([
            'message' => 'Data Siswa Berhasil Diubah',
            'data' => $request->user(),
        ], 200);
    }
}
