<?php

namespace App\Http\Controllers\API\Auth\Teacher;

use App\Models\Teacher;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function create(Request $request)
    {
        $data = $request->validate([
            'id_school_level' => 'required',
            'id_bank' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8',
            'long_name' => 'required|string',
            'address_coordinat' => 'required',
            'age' => 'required|integer',
            'last_education' => 'required',
            'phone_number' => 'required|numeric',
            'gender' => 'required',
            'available' => 'required|boolean',
            'photo' => 'required|mimes:jpeg,png,jpg,svg|max:2048',
            'ktp' => 'required|mimes:jpeg,png,jpg,svg|max:2048',
            'cv' => 'required|mimes:jpeg,png,jpg,svg|max:2048',
            'achievements.image' => 'mimes:jpeg,png,jpg,svg,pdf|max:2048',
        ]);

        $now = date("Y-m-d H:i:s");

        $data['password'] = Hash::make($request->password);
        $data['total_rating'] = 0;
        $slug = Str::slug($request->long_name . ' ' . $now);

        if(request()->file('photo')) {
            $photo = request()->file('photo');
            $photoUrl = $photo->storeAs("APIDocs/teacher/photos", "{$slug}.{$photo->extension()}");
        } else {
            $photoUrl = null;
        }

        if(request()->file('ktp')) {
            $ktp = request()->file('ktp');
            $ktpUrl = $ktp->storeAs("APIDocs/teacher/ktp", "{$slug}.{$ktp->extension()}");
        } else {
            $ktpUrl = null;
        }

        if(request()->file('cv')) {
            $cv = request()->file('cv');
            $cvUrl = $cv->storeAs("APIDocs/teacher/cv", "{$slug}.{$cv->extension()}");
        } else {
            $cvUrl = null;
        }

        $data['photo'] = $photoUrl;
        $data['ktp'] = $ktpUrl;
        $data['cv'] = $cvUrl;

        $teacher = Teacher::create($data);

        if($request->achievements) {
            foreach($request->achievements as $key => $achievement) {
                if($achievement['image']) {
                    $image = $achievement['image'];
                    $imageUrl = $image->storeAs("APIDocs/teacher/achievements", "{$slug}.-.{$key}.{$image->extension()}");
                } else {
                    $imageUrl = null;
                }

                $achievement['image'] = $imageUrl;

                $teacher->teacherAchievements()->create($achievement);
            }
        }

        $teacher->teacherDeposit()->create([
            'total_money' => 0,
        ]);

        return response()->json([
            'message' => 'Data Tentor Berhasil Ditambahkan',
            'data' => $teacher,
        ], 200);
    }
}
