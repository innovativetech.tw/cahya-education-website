<?php

namespace App\Http\Controllers\API\Auth\Teacher;

use App\Models\Teacher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $teacher = Teacher::where('email', $request->email)->first();

        if(!$teacher || !Hash::check($request->password, $teacher->password)) {
            throw ValidationException::withMessages([
                'alert' => ['Email atau password salah.'],
            ]);
        }

        $token = $teacher->createToken('token')->plainTextToken;

        return response()->json([
            'message' => 'success',
            'data' => $teacher,
            'token' => $token,
        ], 200);
    }
}
