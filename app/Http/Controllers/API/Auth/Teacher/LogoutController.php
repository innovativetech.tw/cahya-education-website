<?php

namespace App\Http\Controllers\API\Auth\Teacher;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LogoutController extends Controller
{
    public function logout(Request $request)
    {
        $teacher = $request->user();
        $teacher->currentAccessToken()->delete();

        return response()->json([
            'message' => $teacher->long_name . ' berhasil logout',
        ], 200);
    }
}
