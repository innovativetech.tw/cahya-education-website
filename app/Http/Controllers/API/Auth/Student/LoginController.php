<?php

namespace App\Http\Controllers\API\Auth\Student;

use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $student = Student::where('email', $request->email)->first();

        if(!$student || !Hash::check($request->password, $student->password)) {
            throw ValidationException::withMessages([
                'alert' => ['Email atau password salah.'],
            ]);
        }

        $token = $student->createToken('token')->plainTextToken;

        return response()->json([
            'message' => 'success',
            'data' => $student,
            'token' => $token,
        ], 200);
    }
}
