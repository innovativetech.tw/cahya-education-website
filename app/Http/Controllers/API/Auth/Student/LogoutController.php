<?php

namespace App\Http\Controllers\API\Auth\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LogoutController extends Controller
{
    public function logout(Request $request)
    {
        $student = $request->user();
        $student->currentAccessToken()->delete();

        return response()->json([
            'message' => $student->long_name . ' berhasil logout',
        ], 200);
    }
}
