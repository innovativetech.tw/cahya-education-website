<?php

namespace App\Http\Controllers\API\Auth\Student;

use App\Models\Student;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function create(Request $request)
    {
        $data = $request->validate([
            'id_school_level' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8',
            'long_name' => 'required|string',
            'address' => 'required',
            'school_name' => 'required',
            'class' => 'required',
            'gender' => 'required',
            'photo_student' => 'required|mimes:jpeg,png,jpg,svg|max:2048',
        ]);

        $now = date("Y-m-d H:i:s");

        $data['password'] = Hash::make($request->password);

        $slug = Str::slug($request->long_name . ' ' . $now);

        if(request()->file('photo_student')) {
            $photo = request()->file('photo_student');
            $photoUrl = $photo->storeAs("APIDocs/student/photos", "{$slug}.{$photo->extension()}");
        } else {
            $photoUrl = null;
        }

        $data['photo_student'] = $photoUrl;

        $student = Student::create($data);

        return response()->json([
            'message' => 'Data Siswa Berhasil Ditambahkan',
            'data' => $student,
        ], 200);
    }
}
