<?php

namespace App\Http\Controllers\API\Auth\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function create(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);

        $data['password'] = Hash::make($request->password);
        $data['is_admin'] = true;

        $student = Admin::create($data);

        return response()->json([
            'message' => 'Data Admin Berhasil Ditambahkan',
            'data' => $student,
        ], 200);
    }
}
