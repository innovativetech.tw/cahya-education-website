<?php

namespace App\Http\Controllers\API\Auth\Admin;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $admin = Admin::where('email', $request->email)->first();

        if(!$admin || !Hash::check($request->password, $admin->password)) {
            throw ValidationException::withMessages([
                'alert' => ['Email atau password salah.'],
            ]);
        }

        $token = $admin->createToken('token')->plainTextToken;

        return response()->json([
            'message' => 'success',
            'data' => $admin,
            'token' => $token,
        ], 200);
    }
}
