<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaketOrderTeacher extends Model
{
    use HasFactory;

    public function paketOrderStudents()
    {
        return $this->belongsToMany(PaketOrderStudent::class);
    }

    public function teachers()
    {
        return $this->belongsToMany(Teacher::class);
    }
}
