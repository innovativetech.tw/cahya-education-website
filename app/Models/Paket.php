<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paket extends Model
{
    use HasFactory;

    public function paketOrderStudent()
    {
        return $this->hasOne(PaketOrderStudent::class, 'id_paket');
    }

    public function schoolLevel()
    {
        return $this->belongsTo(SchoolLevel::class);
    }
}
