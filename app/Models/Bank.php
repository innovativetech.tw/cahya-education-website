<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    use HasFactory;

    public function teacher()
    {
        return $this->hasOne(Teacher::class, 'id_bank');
    }
}
