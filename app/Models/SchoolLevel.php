<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SchoolLevel extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function students()
    {
        return $this->hasMany(Student::class, 'id_school_level');
    }

    public function teachers()
    {
        return $this->hasMany(Teacher::class, 'id_school_level');
    }

    public function pakets()
    {
        return $this->hasMany(Paket::class, 'id_school_level');
    }
}
