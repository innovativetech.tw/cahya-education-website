<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaketOrderStudent extends Model
{
    use HasFactory;

    public function teacherTransactions()
    {
        return $this->hasMany(TeacherTransaction::class, 'id_paket_order_student');
    }

    public function paketOrderTeachers()
    {
        return $this->hasMany(PaketOrderTeacher::class, 'id_paket_order_student');
    }

    public function paket()
    {
        return $this->belongsTo(Paket::class);
    }

    public function students()
    {
        return $this->belongsToMany(Student::class);
    }
}
