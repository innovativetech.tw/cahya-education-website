<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeacherTransaction extends Model
{
    use HasFactory;

    public function teacherDeposit()
    {
        return $this->belongsTo(TeacherDeposit::class);
    }

    public function paketOrderStudents()
    {
        return $this->belongsToMany(PaketOrderStudent::class);
    }
}
