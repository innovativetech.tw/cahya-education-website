<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Student extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $guarded = [];

    public function ratings()
    {
        return $this->hasMany(Rating::class, 'id_student');
    }

    public function schoolLevel()
    {
        return $this->belongsTo(SchoolLevel::class);
    }

    public function paketOrderStudents()
    {
        return $this->hasMany(PaketOrderStudent::class, 'id_student');
    }

    public function locationOrders()
    {
        return $this->hasMany(LocationOrder::class, 'id_student');
    }
}
