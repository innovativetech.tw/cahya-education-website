<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Teacher extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $guarded = [];

    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }

    public function teacherFreeTimes()
    {
        return $this->hasMany(TeacherFreeTime::class, 'id_teacher');
    }

    public function teacherAchievements()
    {
        return $this->hasMany(TeacherAchievement::class, 'id_teacher');
    }

    public function teacherDeposit()
    {
        return $this->hasOne(TeacherDeposit::class, 'id_teacher');
    }

    public function paketOrderTeachers()
    {
        return $this->hasMany(PaketOrderTeacher::class, 'id_teacher');
    }

    public function schoolLevel()
    {
        return $this->belongsTo(SchoolLevel::class);
    }

    public function locationOrders()
    {
        return $this->hasMany(LocationOrder::class, 'id_teacher');
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class, 'id_teacher');
    }
}
