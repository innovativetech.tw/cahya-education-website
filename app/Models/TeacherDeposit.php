<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeacherDeposit extends Model
{
    use HasFactory;

    protected $guarded = ['id_teacher'];

    public function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }

    public function teacherTransactions()
    {
        return $this->hasMany(TeacherTransaction::class, 'id_teacher_deposit');
    }
}
