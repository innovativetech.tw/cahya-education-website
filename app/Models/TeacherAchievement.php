<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeacherAchievement extends Model
{
    use HasFactory;

    protected $guarded = ['id_teacher'];

    public function teachers()
    {
        return $this->belongsTo(Teacher::class);
    }
}
