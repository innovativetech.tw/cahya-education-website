<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeacherTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_transactions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_teacher_deposit')->constrained('teacher_deposits');
            $table->foreignId('id_paket_order_student')->constrained('paket_order_students')->nullable();
            $table->string('description');
            $table->string('transaction_photo')->nullable();
            $table->float('amount');
            $table->enum('type', ['paket', 'withdraw']);
            $table->enum('status', ['belum dibayar', 'sudah dibayar']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_transactions');
    }
}
