<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_school_level')->constrained('school_levels');
            $table->foreignId('id_bank')->constrained('banks');
            $table->string('email');
            $table->string('password');
            $table->string('long_name');
            $table->string('address_coordinat');
            $table->integer('age');
            $table->string('last_education');
            $table->string('phone_number', 14);
            $table->enum('gender', ['laki-laki', 'perempuan']);
            $table->boolean('available');
            $table->float('total_rating');
            $table->string('bank_account')->nullable();
            $table->string('photo');
            $table->string('ktp');
            $table->string('cv');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
