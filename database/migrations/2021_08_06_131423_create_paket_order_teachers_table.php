<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaketOrderTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paket_order_teachers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_teacher')->constrained('teachers');
            $table->foreignId('id_paket_order_student')->constrained('paket_order_students');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paket_order_teachers');
    }
}
