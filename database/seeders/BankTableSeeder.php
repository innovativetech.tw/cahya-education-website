<?php

namespace Database\Seeders;

use App\Models\Bank;
use Illuminate\Database\Seeder;

class BankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $banks = [
            [
                'name' => 'BRI',
                'photo_path' => 'tes.jpg',
            ],
            [
                'name' => 'Mandiri',
                'photo_path' => 'tes.jpg',
            ],
            [
                'name' => 'BNI',
                'photo_path' => 'tes.jpg',
            ],
            [
                'name' => 'BCA',
                'photo_path' => 'tes.jpg',
            ],
        ];
        
        foreach ($banks as $bank) {
            Bank::create($bank);
        }
    }
}
