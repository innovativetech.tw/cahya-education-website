<?php

namespace Database\Seeders;

use App\Models\SchoolLevel;
use Illuminate\Database\Seeder;

class SchoolLevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $schoolLevels = [
            [
                'name' => 'ngaji',
            ],
            [
                'name' => 'tk',
            ],
            [
                'name' => 'sd',
            ],
            [
                'name' => 'smp',
            ],
            [
                'name' => 'sma',
            ],
        ];
        
        foreach ($schoolLevels as $schoolLevel) {
            SchoolLevel::create($schoolLevel);
        }
    }
}
