<?php

use App\Http\Controllers\API\Auth\Admin\RegisterController as AdminRegisterController;
use App\Http\Controllers\API\Auth\Admin\LoginController as AdminLoginController;
use App\Http\Controllers\API\Auth\Admin\LogoutController as AdminLogoutController;
use App\Http\Controllers\API\Auth\Student\LoginController as StudentLoginController;
use App\Http\Controllers\API\Auth\Student\LogoutController as StudentLogoutController;
use App\Http\Controllers\API\Auth\Student\RegisterController as StudentRegisterController;
use App\Http\Controllers\API\Auth\Teacher\LoginController as TeacherLoginController;
use App\Http\Controllers\API\Auth\Teacher\LogoutController as TeacherLogoutController;
use App\Http\Controllers\API\Auth\Teacher\RegisterController as TeacherRegisterController;
use App\Http\Controllers\API\StudentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('siswa')->name('student.')->group(function () {
    Route::post('/daftar', [StudentRegisterController::class, 'create'])->name('register');
    Route::post('/masuk', [StudentLoginController::class, 'login'])->name('login');
    Route::middleware(['auth:sanctum'])->group(function () {
        Route::get('/logout', [StudentLogoutController::class, 'logout'])->name('logout');
        Route::post('/profil/update', [StudentController::class, 'update'])->name('update');
    });
});

Route::prefix('tentor')->name('teacher.')->group(function () {
    Route::post('/daftar', [TeacherRegisterController::class, 'create'])->name('register');
    Route::post('/masuk', [TeacherLoginController::class, 'login'])->name('login');
    Route::middleware(['auth:sanctum'])->group(function () {
        Route::get('/logout', [TeacherLogoutController::class, 'logout'])->name('logout');
    });
});

Route::prefix('admin')->name('admin.')->group(function () {
    Route::post('/daftar', [AdminRegisterController::class, 'create'])->name('register');
    Route::post('/masuk', [AdminLoginController::class, 'login'])->name('login');
    Route::middleware(['auth:sanctum'])->group(function () {
        Route::get('/logout', [AdminLogoutController::class, 'logout'])->name('logout');
    });
});